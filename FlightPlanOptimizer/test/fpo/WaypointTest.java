package fpo;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michelle
 */
public class WaypointTest {
    
    public WaypointTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of default constructor of class Waypoint.
     */
    @Test
    public void testDefaultConstructor() {
        System.out.println("testDefaultConstructor");
        Waypoint instance = new Waypoint();
        double result = instance.getLat();
        assertTrue(result > -89.99 && result <= 89.99);
        result = instance.getLon();
        assertTrue(result > -89.99 && result <= 89.99);
        result = instance.getAlt();
        assertTrue(result > 0 && result <= 40000);
    }
    
    /**
     * Test of initialized constructor of class Waypoint.
     */
    @Test
    public void testInitConstructor() {
        System.out.println("testInitConstructor");
        Waypoint instance = new Waypoint(-54.0, -70.9, 39999);
        double expResult = -54.0;
        double result = instance.getLat();
        assertEquals(expResult, result, 0.0);
        expResult = -70.9;
        result = instance.getLon();
        assertEquals(expResult, result, 0.0);
        expResult = 39999;
        result = instance.getAlt();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of 2-value initialized constructor of class Waypoint.
     */
    @Test
    public void test2ValueInitConstructor() {
        System.out.println("testInitConstructor");
        Waypoint instance = new Waypoint(-54.0, -70.9);
        double expResult = -54.0;
        double result = instance.getLat();
        assertEquals(expResult, result, 0.0);
        expResult = -70.9;
        result = instance.getLon();
        assertEquals(expResult, result, 0.0);
        expResult = 0;
        result = instance.getAlt();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLat method, of class Waypoint.
     */
    @Test
    public void testGetLat() {
        System.out.println("getLat");
        Waypoint instance = new Waypoint(34.0, 83.0, 300);
        double expResult = 34.0;
        double result = instance.getLat();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getLon method, of class Waypoint.
     */
    @Test
    public void testGetLon() {
        System.out.println("getLon");
        Waypoint instance = new Waypoint(34.0, 83.0, 300);
        double expResult = 83.0;
        double result = instance.getLon();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getAlt method, of class Waypoint.
     */
    @Test
    public void testGetAlt() {
        System.out.println("getAlt");
        Waypoint instance = new Waypoint(34.0, 83.0, 300);
        double expResult = 300.0;
        double result = instance.getAlt();
        assertEquals(expResult, result, 0.0);
    }
    
    /**
     * Test of distanceTo method, of class Waypoint.
     */
    @Test
    public void testDistanceTo() {
        // From http://www.movable-type.co.uk/scripts/latlong.html
        // Distance ignoring altitude is 92.18 km = 57.28 mi
        System.out.println("distanceTo");
        Waypoint endPoint = new Waypoint(34.0, 84.0, 300);
        Waypoint instance = new Waypoint(34.0, 83.0, 300);
        double expResult = 57.28;
        double result = instance.distanceTo(endPoint);
        assertEquals(expResult, result, 0.01);
    }

    /**
     * Test of toString method, of class Waypoint.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Waypoint instance = new Waypoint(34.0, 83.0, 300);
        String expResult = "Lat = 34.0; Lon = 83.0; Alt = 300.0";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Waypoint.
     */
    @Test
    public void testEquals() {
        System.out.println("equals");
        Waypoint instance = new Waypoint();
        Waypoint compareto = new Waypoint(instance.getLat(), instance.getLon(), instance.getAlt());
        
        // First, compare it to itself
        boolean expResult = true;
        boolean result = instance.equals(instance);
        assertEquals(expResult, result);
        
        // Next, compare it to a different waypoint with the same coordinates
        result = instance.equals(compareto);
        assertEquals(expResult, result);
        
        // Now, compare to a blank flight plan object
        FlightPlan fltpln = new FlightPlan();
        expResult = false;
        result = instance.equals(fltpln);
        assertEquals(expResult, result);
        
        // Last, compare to a waypoint with different coordinates
        Waypoint newWypt = new Waypoint();
        expResult = false;
        result = instance.equals(newWypt);
        assertEquals(expResult, result);
    }

    /**
     * Test of hashCode method, of class Waypoint.
     */
    @Test
    public void testHashCode() {
        System.out.println("hashCode");
        Waypoint instance = new Waypoint();
        int expResult = 0;
        int result = instance.hashCode();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLat method, of class Waypoint.
     */
    @Test
    public void testSetLat() {
        System.out.println("setLat");
        double lat = 0.0;
        Waypoint instance = new Waypoint();
        instance.setLat(lat);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLon method, of class Waypoint.
     */
    @Test
    public void testSetLon() {
        System.out.println("setLon");
        double lon = 0.0;
        Waypoint instance = new Waypoint();
        instance.setLon(lon);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAlt method, of class Waypoint.
     */
    @Test
    public void testSetAlt() {
        System.out.println("setAlt");
        double alt = 0.0;
        Waypoint instance = new Waypoint();
        instance.setAlt(alt);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
