/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fpo;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Michelle
 */
public class FlightPlanTest {
    public static FlightPlan flightPlan = null;
        
    public FlightPlanTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        //  Start on the ground in Warner Robins, GA
        Waypoint waypoint = new Waypoint(32.613001, -83.624201, 0);
        FltPlanManager.addWypt(waypoint);
        // Fly over Ft. Walton Beach at 1000 ft
        Waypoint waypoint2 = new Waypoint(30.420071, -86.617031, 1000);
        FltPlanManager.addWypt(waypoint2);
        // Land long enough to say hi to my folks in New Orleans
        Waypoint waypoint3 = new Waypoint(29.994092, -90.241743, 0);
        FltPlanManager.addWypt(waypoint3);
        // Say hi to Uncle Tim in Phoenix and show him this prototype
        Waypoint waypoint4 = new Waypoint(33.448377, -112.074037, 0);
        FltPlanManager.addWypt(waypoint4);
        // Fly over the Grand Canyon at 1000 ft
        Waypoint waypoint5 = new Waypoint(36.054445, -112.140111, 1000);
        FltPlanManager.addWypt(waypoint5);
                
        flightPlan = new FlightPlan();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of generateIndividual method, of class FlightPlan.
     */
    @Test
    public void testGenerateIndividual() {
        System.out.println("generateIndividual");
        
        flightPlan.generateIndividual();
        
        // Verify the flight plan instance matches the FltPlanManager.  Keep 
        //  in mind they won't be in the same order
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            assertTrue(FltPlanManager.containsWaypoint(flightPlan.getWypt(wyptIndex)));
        }
    }

    /**
     * Test of getWypt method, of class FlightPlan.
     */
    @Test
    public void testGetWypt() {
        System.out.println("getWypt");
        int tourPosition = 0;
        ArrayList<Waypoint> temp = new ArrayList<>();
        
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }
        
        flightPlan = new FlightPlan(temp);
        
        Waypoint expResult = FltPlanManager.getWypt(tourPosition);
        Waypoint result = flightPlan.getWypt(tourPosition);
        assertEquals(expResult, result);
    }

    /**
     * Test of setWypt method, of class FlightPlan.
     */
    @Test
    public void testSetWypt() {
        System.out.println("setWypt");
        int tourPosition = 0;
        Waypoint wypt = new Waypoint(32.0, -83.0, 200);
        FlightPlan instance = flightPlan;
        instance.setWypt(tourPosition, wypt);
        assertEquals(wypt, instance.getWypt(tourPosition));
    }

    /**
     * Test of getDistance method, of class FlightPlan.
     */
    @Test
    public void testGetDistance() {
        // Warner robins to Ft Walton = 374.1 km
        // Ft Walton to NOLA =  	351.5 km
        // NOLA to Phoenix =            2096 km
        // Phoenix to Grand Canyon =    289.8 km
        // Grand Canyon back to WR =    2646 km
        // Total =          5757.4 km = 3577.482502187227 miles
        System.out.println("getDistance");
        
        // Construct the flight plan
        ArrayList<Waypoint> temp = new ArrayList<>();
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }        
        flightPlan = new FlightPlan(temp);
        
        FlightPlan instance = flightPlan;
        double expResult = 3577;
        double result = instance.getDistance();
        assertEquals(expResult, result, 10);
    }

    /**
     * Test of getFitness method, of class FlightPlan.
     */
    @Test
    public void testGetFitness() {
        System.out.println("getFitness");
        
        // Construct the flight plan
        ArrayList<Waypoint> temp = new ArrayList<>();
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }        
        flightPlan = new FlightPlan(temp);
        
        double expResult = 1 / 3577;
        double result = flightPlan.getFitness();
        assertEquals(expResult, result, 0.1);
    }

    /**
     * Test of flightPlanSize method, of class FlightPlan.
     */
    @Test
    public void testFlightPlanSize() {
        System.out.println("flightPlanSize");
        
        // Construct the flight plan
        ArrayList<Waypoint> temp = new ArrayList<>();
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }        
        flightPlan = new FlightPlan(temp);
        
        int expResult = 5;
        int result = flightPlan.flightPlanSize();
        assertEquals(expResult, result);
    }

    /**
     * Test of containsWypt method, of class FlightPlan.
     */
    @Test
    public void testContainsWypt() {
        System.out.println("containsWypt");
        Waypoint wypt = new Waypoint(30.420071, -86.617031, 1000);
        
        // Construct the flight plan
        ArrayList<Waypoint> temp = new ArrayList<>();
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }        
        flightPlan = new FlightPlan(temp);
        
        boolean expResult = true;
        boolean result = flightPlan.containsWypt(wypt);
        assertEquals(expResult, result);
        
        // Now let's look for one that isnt here
        expResult = false;
        wypt = new Waypoint(30, -87, 100000);
        result = flightPlan.containsWypt(wypt);
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class FlightPlan.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        
        // Construct the flight plan
        ArrayList<Waypoint> temp = new ArrayList<>();
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
            temp.add(FltPlanManager.getWypt(wyptIndex));
        }        
        flightPlan = new FlightPlan(temp);
        
        String expResult = "|Lat = 32.613001; Lon = -83.624201; Alt = 0.0|";
        expResult = expResult.concat("Lat = 30.420071; Lon = -86.617031; Alt = 1000.0|");
        expResult = expResult.concat("Lat = 29.994092; Lon = -90.241743; Alt = 0.0|");
        expResult = expResult.concat("Lat = 33.448377; Lon = -112.074037; Alt = 0.0|");
        expResult = expResult.concat("Lat = 36.054445; Lon = -112.140111; Alt = 1000.0|");
        String result = flightPlan.toString();
        assertEquals(expResult, result);
    }
    
}
