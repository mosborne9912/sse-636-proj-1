/*
* Waypoint.java
* Models a waypoint
* Used example code from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

import java.lang.UnsupportedOperationException;
import java.util.Random;

public class Waypoint {
    private double lat;
    private double lon;
    private double alt;
    private String label = "";
    
    private final double rangeMin = -89.99;
    private final double rangeMax = 89.99;
    private final double altRangeMin = 0;
    private final double altRangeMax = 40000;
    
    Random r = new Random();
    
    // Constructs a randomly placed waypoint
    public Waypoint(){
        this.lat = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        this.lon = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        this.alt = altRangeMin + (altRangeMax - altRangeMin) * r.nextDouble();
    }
    
    // Constructs a waypoint at chosen x, y location
    public Waypoint(double lat, double lon, double alt){
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
    }
    
    // Constructs a waypoint at chosen x, y location
    public Waypoint(double lat, double lon, double alt, String label){
        this.lat = lat;
        this.lon = lon;
        this.alt = alt;
        this.label = label;
    }
    
    // Constructs a waypoint at chosen x, y location
    public Waypoint(double lat, double lon){
        this.lat = lat;
        this.lon = lon;
        this.alt = 0;
    }
    
    // Gets the distance to given waypoint
    public double distanceTo(Waypoint wypt){
        double R = 6371000; // meters
        double lat1 = Math.toRadians(this.lat);
        double lat2 = Math.toRadians(wypt.lat);
        double lon1 = Math.toRadians(this.lon);
        double lon2 = Math.toRadians(wypt.lon);
        double alt1 = Math.toRadians(this.alt);
        double alt2 = Math.toRadians(wypt.alt);
        double dlat = lat2-lat1;
        double dlon = lon2-lon1;
        double a = Math.sin(dlat/2) * Math.sin(dlat/2) +
            Math.cos(lat1) * Math.cos(lat2) *
            Math.sin(dlon/2) * Math.sin(dlon/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double d = R * c * 6.21371192237334e-4; // 1 meter = 6.21371192237334e-4 mi
        
        return d;
    }
    
    @Override
    public String toString(){
        StringBuilder str = new StringBuilder("Lat = ");
        str.append(this.lat);
        str.append("; Lon = ");
        str.append(this.lon);
        str.append("; Alt = ");
        str.append(this.alt);
        if(!"".equals(this.label)){
            str.append("; Label = ");
            str.append(this.label);
        }
        return str.toString();
    }
    
    @Override
    public boolean equals(Object o){
        Waypoint wypt = null;
        // If the object is compared with itself then return true  
        if (o == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(o instanceof Waypoint)) {
            return false;
        }
        
        wypt = (Waypoint) o;
        if(this.getAlt() == wypt.getAlt() &&
                this.getLat() == wypt.getLat() &&
                this.getLon() == wypt.getLon()) {
            return true;
        }
        
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.lat) ^ (Double.doubleToLongBits(this.lat) >>> 32));
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.lon) ^ (Double.doubleToLongBits(this.lon) >>> 32));
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.alt) ^ (Double.doubleToLongBits(this.alt) >>> 32));
        return hash;
    }

    /**
     * @return the lat
     */
    public double getLat() {
        return this.lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * @return the lon
     */
    public double getLon() {
        return this.lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(double lon) {
        this.lon = lon;
    }

    /**
     * @return the alt
     */
    public double getAlt() {
        return this.alt;
    }

    /**
     * @param alt the alt to set
     */
    public void setAlt(double alt) {
        this.alt = alt;
    }
}