/*
* FltPlanManager.java
* Holds the waypoints of a flight plan
* Used example code  from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

import java.util.ArrayList;

public class FltPlanManager {

    // Holds our waypoints
    private static ArrayList destinationWypt = new ArrayList<Waypoint>();

    // Adds a destination waypoint
    public static void addWypt(Waypoint waypoint) {
        destinationWypt.add(waypoint);
    }
    
    // Get a waypoint
    public static Waypoint getWypt(int index){
        return (Waypoint)destinationWypt.get(index);
    }
    
    // Get the number of destination waypoints
    public static int numberOfWypts(){
        return destinationWypt.size();
    }
    
    public static boolean containsWaypoint(Waypoint wypt) {
        return destinationWypt.contains(wypt);
    }
    
    public static ArrayList getDestinationWaypoints(){
        return destinationWypt;
    }
    
    public static void initializeFlightPlan() {
        // Create and add our waypoints
        //  Start on the ground in Warner Robins, GA
        Waypoint waypoint = new Waypoint(32.613001, -83.624201, 0, "WRGA");
        FltPlanManager.addWypt(waypoint);
        // Land long enough to say hi to my folks in New Orleans
        Waypoint waypoint2 = new Waypoint(29.994092, -90.241743, 0, "NOLA");
        FltPlanManager.addWypt(waypoint2);
        // Fly over the Grand Canyon at 1000 ft
        Waypoint waypoint3 = new Waypoint(36.054445, -112.140111, 1000, "GCan");
        FltPlanManager.addWypt(waypoint3);
        // Fly over Ft. Walton Beach at 1000 ft
        Waypoint waypoint4 = new Waypoint(30.420071, -86.617031, 1000, "FWB");
        FltPlanManager.addWypt(waypoint4);
        // Say hi to Uncle Tim in Phoenix and show him this prototype
        Waypoint waypoint5 = new Waypoint(33.448377, -112.074037, 0, "PHOE");
        FltPlanManager.addWypt(waypoint5);
        // Fly past the Athabasca Glacier in Banff, Alberta, Canada at 3000 ft
        Waypoint waypoint6 = new Waypoint(52.189723, -117.257821, 3000, "Glacier");
        FltPlanManager.addWypt(waypoint6);
        // Fly past Monument Valley, UT at 1000 ft
        Waypoint waypoint7 = new Waypoint(37.004245, -110.173478, 1000, "Monument");
        FltPlanManager.addWypt(waypoint7);
        // Fly past Monticello
        Waypoint waypoint8 = new Waypoint(37.915664, -78.326403, 400, "Monti");
        FltPlanManager.addWypt(waypoint8);
        // Fly over the Cargill Salt Ponds in San Francisco
        Waypoint waypoint9 = new Waypoint(37.50253, -122.21082, 1000, "SanFran");
        FltPlanManager.addWypt(waypoint9);
        // Fly past Mt. Ranier, WA
        Waypoint waypoint10 = new Waypoint(46.8523, -121.76032, 15000, "MtRain");
        FltPlanManager.addWypt(waypoint10);
    }
}