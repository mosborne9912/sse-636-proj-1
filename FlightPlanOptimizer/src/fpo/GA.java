/*
* GA.java
* Manages algorithms for evolving population
* Used example code  from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

public class GA {

    /* GA parameters */
    private static final double mutationRate = 0.015;
    private static final int tournamentSize = 5;
    private static final boolean elitism = true;
    
    private static double prevFitness = 0;
    private static double timeSinceImprovement = 0;

    // Evolves a population over one generation
    public static Population evolvePopulation(Population pop) {
        Population newPopulation = new Population(pop.populationSize(), false);

        // Keep our best individual if elitism is enabled
        int elitismOffset = 0;
        if (elitism) {
            newPopulation.saveFlightPlan(0, pop.getFittest());
            elitismOffset = 1;
        }

        // Crossover population
        // Loop over the new population's size and create individuals from
        // Current population
        for (int i = elitismOffset; i < newPopulation.populationSize(); i++) {
            // Select parents
            FlightPlan parent1 = tournamentSelection(pop);
            FlightPlan parent2 = tournamentSelection(pop);
            // Crossover parents
            FlightPlan child = crossover(parent1, parent2);
            // Add child to new population
            newPopulation.saveFlightPlan(i, child);
        }

        // Mutate the new population a bit to add some new genetic material
        for (int i = elitismOffset; i < newPopulation.populationSize(); i++) {
            mutate(newPopulation.getFlightPlan(i));
        }

        return newPopulation;
    }

    // Applies crossover to a set of parents and creates offspring
    public static FlightPlan crossover(FlightPlan parent1, FlightPlan parent2) {
        // Create new child flight plan
        FlightPlan child = new FlightPlan();

        // Get start and end sub flight plan positions for parent1's flight plan
        int startPos = (int) (Math.random() * parent1.flightPlanSize());
        int endPos = (int) (Math.random() * parent1.flightPlanSize());

        // Loop and add the sub flight plan from parent1 to our child
        for (int i = 0; i < child.flightPlanSize(); i++) {
            // If our start position is less than the end position
            if (startPos < endPos && i > startPos && i < endPos) {
                child.setWypt(i, parent1.getWypt(i));
            } // If our start position is larger
            else if (startPos > endPos) {
                if (!(i < startPos && i > endPos)) {
                    child.setWypt(i, parent1.getWypt(i));
                }
            }
        }

        // Loop through parent2's flight plan
        for (int i = 0; i < parent2.flightPlanSize(); i++) {
            // If child doesn't have the waypoint add it
            if (!child.containsWypt(parent2.getWypt(i))) {
                // Loop to find a spare position in the child's flight plan
                for (int ii = 0; ii < child.flightPlanSize(); ii++) {
                    // Spare position found, add waypoint
                    if (child.getWypt(ii) == null) {
                        child.setWypt(ii, parent2.getWypt(i));
                        break;
                    }
                }
            }
        }
        return child;
    }

    // Mutate a flight plan using swap mutation
    private static void mutate(FlightPlan flightPlan) {
        // Loop through flight plan waypoints
        for(int fplnPos1=0; fplnPos1 < flightPlan.flightPlanSize(); fplnPos1++){
            // Apply mutation rate
            if(Math.random() < mutationRate){
                // Get a second random position in the flight plan
                int fplnPos2 = (int) (flightPlan.flightPlanSize() * Math.random());

                // Get the waypoitns at target position in flight plan
                Waypoint wypt1 = flightPlan.getWypt(fplnPos1);
                Waypoint wypt2 = flightPlan.getWypt(fplnPos2);

                // Swap them around
                flightPlan.setWypt(fplnPos2, wypt1);
                flightPlan.setWypt(fplnPos1, wypt2);
            }
        }
    }

    public static boolean isFitEnough(Population pop) {
        boolean retVal = false;
        
        // Get the current fitness
        double currentFitness = pop.getFittest().getFitness();
        
        // Has there been a change in fitness?
        if(currentFitness != prevFitness){
            // Yes, so reset the clock
            timeSinceImprovement = 0;
        } else {
            // No, so increment the timer
            timeSinceImprovement++;
        }
        
        // If there hasn't been any improvement in 10 generations, 
        //  it's time to call it quits
        if(timeSinceImprovement > 50){
            retVal = true;
        }
        
        // Update the previous value
        prevFitness = currentFitness;
        
        return retVal;
    }
    
    // Selects candidate flight plan for crossover
    private static FlightPlan tournamentSelection(Population pop) {
        // Create a tournament population
        Population tournament = new Population(tournamentSize, false);
        // For each place in the tournament get a random candidate flight plan and
        // add it
        for (int i = 0; i < tournamentSize; i++) {
            int randomId = (int) (Math.random() * pop.populationSize());
            tournament.saveFlightPlan(i, pop.getFlightPlan(randomId));
        }
        // Get the fittest flight plan
        FlightPlan fittest = tournament.getFittest();
        return fittest;
    }
    
}