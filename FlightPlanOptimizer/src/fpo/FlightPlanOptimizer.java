/*
* TSP_GA.java
* Create a tour and evolve a solution
* Used example code  from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

public class FlightPlanOptimizer {

    public static void main(String[] args) {

        FltPlanManager.initializeFlightPlan();

        // Print the very first distance
        FlightPlan temp = new FlightPlan(FltPlanManager.getDestinationWaypoints());
        System.out.println("Initial distance: " + temp.getDistance());
        
        // Initialize population
        Population pop = new Population(300, true);
        System.out.println("First generation distance: " + pop.getFittest().getDistance());

        // Evolve population
        int generationCount = 0;
        while (generationCount < 1000 && !GA.isFitEnough(pop)) {
            pop = GA.evolvePopulation(pop);
            generationCount++;
            System.out.println("Generation: " + generationCount + " Distance: " +
                    pop.getFittest().getDistance() + " Fitness: " + 
                    pop.getFittest().getFitness());
        }

        // Print final results
        System.out.println("Finished");
        System.out.println("Final distance: " + pop.getFittest().getDistance());
        System.out.println("Solution:");
        System.out.println(pop.getFittest().toString());
    }
}