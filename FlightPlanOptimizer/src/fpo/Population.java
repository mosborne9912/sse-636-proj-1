/*
* Population.java
* Manages a population of candidate flightPlans
* Used example code  from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

public class Population {

    // Holds population of flightPlans
    FlightPlan[] flightPlans;

    // Construct a population
    public Population(int populationSize, boolean initialize) {
        flightPlans = new FlightPlan[populationSize];
        // If we need to initialize a population of flightPlans do so
        if (initialize) {
            // Loop and create individuals
            for (int i = 0; i < populationSize(); i++) {
                FlightPlan newTour = new FlightPlan();
                newTour.generateIndividual();
                saveFlightPlan(i, newTour);
            }
        }
    }
    
    // Saves a tour
    public void saveFlightPlan(int index, FlightPlan flightPlan) {
        flightPlans[index] = flightPlan;
    }
    
    // Gets a tour from population
    public FlightPlan getFlightPlan(int index) {
        return flightPlans[index];
    }

    // Gets the best tour in the population
    public FlightPlan getFittest() {
        FlightPlan fittest = flightPlans[0];
        // Loop through individuals to find fittest
        for (int i = 1; i < populationSize(); i++) {
            if (fittest.getFitness() <= getFlightPlan(i).getFitness()) {
                fittest = getFlightPlan(i);
            }
        }
        return fittest;
    }

    // Gets population size
    public int populationSize() {
        return flightPlans.length;
    }
}