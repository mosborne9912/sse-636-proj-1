/*
* FlightPlan.java
* Stores a candidate flightPlan
* Used example code  from http://www.theprojectspot.com/tutorial-post/applying-a-genetic-algorithm-to-the-travelling-salesman-problem/5
*/

package fpo;

import java.util.ArrayList;
import java.util.Collections;

public class FlightPlan {

    // Holds our flightPlan of waypoints
    private ArrayList flightPlan = new ArrayList<Waypoint>();
    // Cache
    private double fitness = 0;
    private double distance = 0;
    
    // Constructs a blank flightPlan
    public FlightPlan(){
        for (int i = 0; i < FltPlanManager.numberOfWypts(); i++) {
            flightPlan.add(null);
        }
    }
    
    public FlightPlan(ArrayList flightPlan){
        this.flightPlan = flightPlan;
    }

    // Creates a random individual
    public void generateIndividual() {
        // Loop through all our destination waypoints and add them to our flight plan
        for (int wyptIndex = 0; wyptIndex < FltPlanManager.numberOfWypts(); wyptIndex++) {
          setWypt(wyptIndex, FltPlanManager.getWypt(wyptIndex));
        }
        // Randomly reorder the flightPlan
        Collections.shuffle(flightPlan);
    }

    // Gets a waypoint from the flightPlan
    public Waypoint getWypt(int flightPlanPosition) {
        return (Waypoint)flightPlan.get(flightPlanPosition);
    }

    // Sets a waypoint in a certain position within a flightPlan
    public void setWypt(int flightPlanPosition, Waypoint waypoint) {
        flightPlan.set(flightPlanPosition, waypoint);
        // If the flightPlans been altered we need to reset the fitness and distance
        fitness = 0;
        distance = 0;
    }
    
    // Gets the flightPlans fitness
    public double getFitness() {
        if (fitness == 0) {
            fitness = 1/getDistance() * 1e4;
        }
        return fitness;
    }
    
    // Gets the total distance of the flightPlan
    public double getDistance(){
        if (distance == 0) {
            int flightPlanDistance = 0;
            // Loop through our flightPlan's waypoints
            for (int wyptIndex=0; wyptIndex < flightPlanSize(); wyptIndex++) {
                // Get waypoint we're travelling from
                Waypoint fromWypt = getWypt(wyptIndex);
                // Waypoint we're travelling to
                Waypoint destinationWypt;
                // Check we're not on our flightPlan's last waypoint, if we are set our
                // flightPlan's final destination waypoint to our starting waypoint
                if(wyptIndex+1 < flightPlanSize()){
                    destinationWypt = getWypt(wyptIndex+1);
                }
                else{
                    destinationWypt = getWypt(0);
                }
                // Get the distance between the two waypoints
                flightPlanDistance += fromWypt.distanceTo(destinationWypt);
            }
            distance = flightPlanDistance;
        }
        return distance;
    }

    // Get number of waypoints on our flightPlan
    public int flightPlanSize() {
        return flightPlan.size();
    }
    
    // Check if the flightPlan contains a waypoint
    public boolean containsWypt(Waypoint wypt){
        return flightPlan.contains(wypt);
    }
    
    @Override
    public String toString() {
        String geneString = "|";
        for (int i = 0; i < flightPlanSize(); i++) {
            geneString += getWypt(i).toString()+"|";
        }
        return geneString;
    }
}